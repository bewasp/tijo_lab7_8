package pl.edu.pwsztar.domain.files;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FileDto;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.FileInputStream;


@Component
public class CsvFileGeneratorImpl implements CsvFileGenerator {
    @Override
    public InputStreamResource toCsv(FileDto fileDto) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileDto.getFile())));
        bufferedWriter.write("Year, Title");
        bufferedWriter.newLine();
        fileDto.getMovies().forEach(movie -> {
            try {
                bufferedWriter.write(movie.getYear() + "," + movie.getTitle().replaceAll(",", " "));
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        bufferedWriter.close();
        return new InputStreamResource(new FileInputStream(fileDto.getFile()));
    }
}
