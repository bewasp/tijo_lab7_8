package pl.edu.pwsztar.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import java.io.IOException;

@Service
public interface FileService {
    InputStreamResource returnTxt() throws IOException;
    InputStreamResource returnCsv() throws IOException;
}
